#!/usr/bin/expect
set timeout 10 
set cmd [lindex $argv 0] 
puts "author:车江毅,https://gitee.com/chejiangyi/jssh"
if { "$cmd" == "ssh" } {
	set userhost [lindex $argv 1] 
	set password [lindex $argv 2] 
	spawn ssh $userhost
	expect {
	"yes/no"
	{send "yes\r"; exp_continue;}
	"password:"
	{send "$password\r";}
	}
	interact
} elseif { "$cmd" == "scp" } {
	set from [lindex $argv 1] 
	set to [lindex $argv 2] 
	set password [lindex $argv 3]
	spawn scp $from $to
	expect {
	"yes/no"
	{send "yes\r"; exp_continue;}
	"password:"
	{send "$password\r";}
	}
	interact

}
